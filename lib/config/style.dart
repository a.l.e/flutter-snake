import 'dart:ui';
import 'package:flame/palette.dart';
import 'package:snake/config/config.dart' as config;

final white = BasicPalette.white.paint();
final blue = BasicPalette.blue.paint();
final red = BasicPalette.red.paint();

 final snakeHead = BasicPalette.lightGreen.paint()
    ..style = PaintingStyle.fill;