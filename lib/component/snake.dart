import 'dart:ui';

import 'package:flame/components.dart';
import 'package:snake/config/config.dart' as config;
import 'package:snake/config/style.dart' as style;
import 'package:snake/game/snake_game.dart';

class Snake extends Component with HasGameRef<SnakeGame> {
  List<int> _snakeHead = [];

  void gameStart() {
    _snakeHead = [
      config.columns ~/ 2,
      config.rows ~/ 2
    ];
  }

  @override
  void render(Canvas canvas) {
    final cellSize = gameRef.gameArea.cellSize;

    final position = Offset(
      gameRef.gameArea.topLeft.x  + _snakeHead[0] * cellSize,
      gameRef.gameArea.topLeft.y  + _snakeHead[1] * cellSize
    );

    canvas.drawRect(position & Size.square(cellSize), style.snakeHead);
  }
}
