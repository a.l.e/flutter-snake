import 'dart:ui';

import 'package:flame/components.dart';
import 'package:flame/palette.dart';
import 'package:snake/config/config.dart' as config;
import 'package:snake/config/style.dart' as style;
import 'package:snake/game/snake_game.dart';

class Grid extends Component with HasGameRef<SnakeGame> {
  Offset _topLeft = Offset.zero;
  Offset _bottomRight = Offset.zero;
  late final double _cellSize;
  late final Rect _canvasRectangle;

  @override
  Future<void> onLoad() async {
    super.onLoad();
    _topLeft = gameRef.gameArea.topLeft.toOffset();
    _bottomRight = gameRef.gameArea.bottomRight.toOffset();
    _cellSize = gameRef.gameArea.cellSize;
    _canvasRectangle = Rect.fromPoints(
      Offset.zero,
      Offset(gameRef.canvasSize.x, gameRef.canvasSize.y)
    );
  }

  @override
  void render(Canvas canvas) {
    _drawBackground(canvas);
    _drawVerticalLines(canvas);
    _drawHorizontalLines(canvas);
  }

  void _drawBackground(Canvas canvas) {
    canvas.drawRect(
      _canvasRectangle,
      BasicPalette.white.paint()
    );
  }

  _drawVerticalLines(Canvas canvas) {
    var x = _topLeft.dx;
    for (int i = 0; i <= config.columns; i++, x += _cellSize) {
      canvas.drawLine(Offset(x, _topLeft.dy), Offset(x, _bottomRight.dy), style.blue);
    }
  }

  _drawHorizontalLines(Canvas canvas) {
    var y = _topLeft.dy;
    for (int i = 0; i <= config.rows; i++, y += _cellSize) {
      canvas.drawLine(Offset(_topLeft.dx, y), Offset(_bottomRight.dx, y), style.blue);
    }
  }
}
