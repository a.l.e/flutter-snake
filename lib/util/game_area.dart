import 'package:flame/components.dart';
import 'package:snake/config/config.dart' as config;

class GameArea {
  late final double cellSize;
  late Vector2 canvasSize;
  late final Vector2 topLeft;
  late final Vector2 bottomRight;

  GameArea(Vector2 canvasSize) {
    canvasSize = canvasSize;

    if (canvasSize.x / config.columns * config.rows < canvasSize.y) {
      cellSize = canvasSize.x / config.columns;
    } else {
      cellSize = canvasSize.y / config.rows;
    }

    final gameWidth = cellSize * config.columns;
    final gameHeight = cellSize * config.rows;

    final leftPad = (canvasSize.x - gameWidth) / 2;
    final topPad = (canvasSize.y - gameHeight) / 2;

    topLeft = Vector2(leftPad, topPad);
    bottomRight = Vector2(leftPad + gameWidth, topPad + gameHeight);
  }
}
