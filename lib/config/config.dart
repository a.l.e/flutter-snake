const rows = 24;
const columns = 12;

/// Snake is composed of bunch of squares and this config defines the thickness of the lines
const snakeStrokeWidth = 4.0;