import 'package:flame/flame.dart';
import 'package:flame/game.dart';
import 'package:flutter/material.dart';
import 'package:snake/component/grid.dart';
import 'package:snake/component/snake.dart';
import 'package:snake/util/game_area.dart';

class SnakeGame extends FlameGame {
  late final GameArea gameArea;

  @override
  Future<void> onLoad() async {
    await super.onLoad();

    WidgetsFlutterBinding.ensureInitialized();
    Flame.device.fullScreen();
    Flame.device.setPortrait();

    gameArea = GameArea(canvasSize);

    final grid = Grid();
    final snake = Snake();
    snake.gameStart();

    add(grid);
    add(snake);
  }
}
